import React from 'react'
import './About.css'
const About = () => {
    return (
        <div className='set_text'>
            I am pursuing a bachelor's from SMIU focusing on computer science and having 2 years of experience as a working professional with a keen interest in Artificial Intelligence, Data Sciences, Backend Engineering, Infrastructure Engineering, DevOps, and Large Scale Distributed Systems. My obsession is learning about emerging technologies like Quantum Computing, and metaverse, and learning about other domains like cosmology, and neurology.
            <br />
            <br />
            I specialize in modern and widely-used technologies, such as:
            <br />
            ✔ Artificial Intelligence: Transformers, Attention, Language Modeling, Sklearn, Tensorflow & PyTorch in GANs, RNN, CNN, BERT;
            <br /> ✔ Front-end Related Technologies: JS ES6+, HTML 5, CSS 3, SVG, Webpack, Babel, Eslint, d3.js;
            <br />✔ Back-end: Node.js, Typescript, Flask, Django;
            <br />✔ Back-end Technologies: REST APIs, WebSockets, Message Queues, GraphQL;
            <br />✔ Version control: Git and Github;
            <br />✔ Database technology and tools: MongoDB, MySQL, PostgreSQL, DynamoDB, Redis;
            <br />✔ Cloud computing services: AWS, GCP;
            <br />✔ DevOps: Docker, Kubernetes, Terraform, Github Actions, AWS DevOps, GitLab;
            <br />✔ Realtime Systems: WebRTC, Sockets;
        </div>
    )
}
export default About